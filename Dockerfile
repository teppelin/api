FROM node:alpine

WORKDIR /api

COPY package.json yarn.lock ./
RUN yarn install --frozen-lockfile --no-cache --production

COPY . /api/

EXPOSE 3000
CMD [ "node", "index.js" ]
