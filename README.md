# ARCHIVED: GraphQL is dogshit.

## api

teppelin api

## Usage

### Install

```
yarn install
```

### Running

Set the following environment variables:

| env var           | description                                                                                                                                    |
| ----------------- | ---------------------------------------------------------------------------------------------------------------------------------------------- |
| DATABASE_URL | The DB owner (root) [connection URI](https://www.postgresql.org/docs/current/libpq-connect.html#LIBPQ-CONNSTRING)                                     |
| AUTH_DATABASE_URL | Limited user who can switch to the visitor role, [connection URI](https://www.postgresql.org/docs/current/libpq-connect.html#LIBPQ-CONNSTRING) |
| REDIS_URL         | Redis URI connection string                                                                                                                    |
| ROOT_URL          | Root (base) API URL                                                                                                                            |
| NATIVE_URI        | Native application success redirect URI, e.g. `com.example.app:/oauth2redirect`, users get redirected here after a successfull OAuth2 login.   |
| FRONTEND_URI      | Frontend HTTP(s) URL, e.g. `https://app.example.com/oauth2redirect` (no trailing slash)                                                        |
| PORT              | Koa listen port, default to `3000`                                                                                                             |
| SECRET            | JWT signing secret                                                                                                                             |
| CORS_ORIGIN       | CORS allow header string, can be a wildcard (`*`) or a host, if not set uses the request `Origin` header                                       |
| NODE_ENV          | app environment, `process.env.NODE_ENV === 'development'`                                                                                      |
| GITHUB_KEY        | GitHub OAuth2 app client ID                                                                                                                    |
| GITHUB_SECRET     | GitHub OAuth2 app client secret                                                                                                                |
| DISCORD_KEY       | Discord OAuth2 app client ID                                                                                                                   |
| DISCORD_SECRET    | Discord OAuth2 app client secret                                                                                                               |
| GOOGLE_KEY        | Google OAuth2 app client ID                                                                                                                    |
| GOOGLE_SECRET     | Google OAuth2 app client secret                                                                                                                |
| ANILIST_KEY       | AniList OAuth2 app client ID                                                                                                                   |
| ANILIST_SECRET    | AniList OAuth2 app client secret                                                                                                               |
| CLOUDFLARE        | [Truthy](https://dorey.github.io/JavaScript-Equality-Table/) string if you want to use the `CF-Connecting-IP` header for ratelimiting          |

- GitHub and AniList don't support multiple callback URLs (native app oauth doesn't work for now)
- AniList doesn't return support scopes => doesn't return an OID profile object

Then run using `node index`

### Docker

Build..

```sh
docker build -t api .
```

Run..

```sh
# maybe pass an env file to 'docker run'
docker run -e DATABASE_URL=... -p 3000:3000 api
```
