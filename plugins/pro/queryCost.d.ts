import { PostGraphileBaseContext, ValidationRule } from "./interfaces";
export interface PostGraphileQueryCostContext extends PostGraphileBaseContext {
    costLimit: number;
    onComplete?: (cost: number, realCost: number) => void;
}
declare const _default: (graphileContext: PostGraphileQueryCostContext) => ValidationRule;
export default _default;
