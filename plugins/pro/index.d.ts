import PgCostsPlugin from "./PgCostsPlugin";
import PgForceConnectionLimitPlugin from "./PgForceConnectionLimitPlugin";
import { Pool } from "pg";
import { PostGraphilePlugin } from "postgraphile";
export { PgCostsPlugin, PgForceConnectionLimitPlugin };
declare module "postgraphile" {
    interface PostGraphileRequestOverridableOptions {
        defaultPaginationCap?: number;
        graphqlDepthLimit?: number;
        graphqlCostLimit?: number;
        exposeGraphQLCost?: boolean;
    }
    interface PostGraphileOptions<Request, Response> {
        defaultPaginationCap?: number;
        graphqlDepthLimit?: number;
        graphqlCostLimit?: number;
        exposeGraphQLCost?: boolean;
        readOnlyConnection?: string | Pool;
        license?: string;
        overrideOptionsForRequest?: (req: Request, meta: {
            options: PostGraphileOptions<Request, Response>;
        }) => PostGraphileRequestOverridableOptions | null;
    }
}
declare const plugin: PostGraphilePlugin;
export default plugin;
