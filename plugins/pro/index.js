module.exports = (function (e) {
  var t = {};
  function n(r) {
    if (t[r]) return t[r].exports;
    var i = (t[r] = { i: r, l: !1, exports: {} });
    return e[r].call(i.exports, i, i.exports, n), (i.l = !0), i.exports;
  }
  return (
    (n.m = e),
    (n.c = t),
    (n.d = function (e, t, r) {
      n.o(e, t) || Object.defineProperty(e, t, { enumerable: !0, get: r });
    }),
    (n.r = function (e) {
      "undefined" != typeof Symbol &&
        Symbol.toStringTag &&
        Object.defineProperty(e, Symbol.toStringTag, { value: "Module" }),
        Object.defineProperty(e, "__esModule", { value: !0 });
    }),
    (n.t = function (e, t) {
      if ((1 & t && (e = n(e)), 8 & t)) return e;
      if (4 & t && "object" == typeof e && e && e.__esModule) return e;
      var r = Object.create(null);
      if (
        (n.r(r),
        Object.defineProperty(r, "default", { enumerable: !0, value: e }),
        2 & t && "string" != typeof e)
      )
        for (var i in e)
          n.d(
            r,
            i,
            function (t) {
              return e[t];
            }.bind(null, i)
          );
      return r;
    }),
    (n.n = function (e) {
      var t =
        e && e.__esModule
          ? function () {
              return e.default;
            }
          : function () {
              return e;
            };
      return n.d(t, "a", t), t;
    }),
    (n.o = function (e, t) {
      return Object.prototype.hasOwnProperty.call(e, t);
    }),
    (n.p = ""),
    n((n.s = 4))
  );
})([
  function (e, t) {
    e.exports = require("graphql");
  },
  function (e, t, n) {
    "use strict";
    Object.defineProperty(t, "__esModule", { value: !0 });
    const r = n(2);
    t.default = function (e, t) {
      if (!e.calculatedVariableValues) {
        const { operationName: n, variables: i } = e,
          o = t.getSchema(),
          a = t
            .getDocument()
            .definitions.find(
              (e) =>
                "OperationDefinition" === e.kind &&
                (!n || (!!e.name && e.name.value === n))
            );
        if (!a) throw new Error(`Could not find operation named '${n}'`);
        const { errors: s, coerced: l } = r.getVariableValues(
          o,
          [...(a.variableDefinitions || [])],
          i || {}
        );
        e.calculatedVariableValues = { errors: s, coerced: l };
      }
      return e.calculatedVariableValues;
    };
  },
  function (e, t) {
    e.exports = require("graphql/execution/values");
  },
  function (e, t, n) {
    "use strict";
    Object.defineProperty(t, "__esModule", { value: !0 });
    const r = n(2);
    t.getArgumentValues = r.getArgumentValues;
  },
  function (e, t, n) {
    "use strict";
    Object.defineProperty(t, "__esModule", { value: !0 });
    const r = n(5),
      i = n(6),
      o = n(7),
      a = n(8),
      s = n(9);
    t.PgCostsPlugin = s.default;
    const l = n(10);
    t.PgForceConnectionLimitPlugin = l.default;
    const u = n(11),
      p = n(12),
      c = 36e5,
      d = 24 * c,
      f = (e) => Buffer.from(String(e), "base64"),
      g =
        "-----BEGIN PUBLIC KEY-----\nMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEiapOpoQOUtpoIoJjCZPyAiX51aIt\nwMJFV4OIttJG7N7EVGOAsgLddp02m8M35S82aHwCHW/AqeBbDpcCVvKGsQ==\n-----END PUBLIC KEY-----";
    function h(e) {
      return { licensedTo: "me" };
    }
    function m(e) {
      return (
        (e &&
          "function" == typeof e.constructor &&
          e.constructor.name &&
          String(e.constructor.name)) ||
        null
      );
    }
    function y(e) {
      return (
        e instanceof u.Pool ||
        !(
          !(t = e) ||
          "function" != typeof t.constructor ||
          t.constructor !== u.Pool.super_
        ) ||
        (!(!e || "object" != typeof e) &&
          ("Pool" === m(e) || "BoundPool" === m(e)) &&
          !!e.Client &&
          !!e.options &&
          "function" == typeof e.connect &&
          "function" == typeof e.end &&
          "function" == typeof e.query)
      );
      var t;
    }
    const v = {
      "cli:flags:add:standard": (e) => (
        e(
          "--read-only-connection <string>",
          "⚡️pass the PostgreSQL connection string to use for read-only queries (i.e. not mutations) - typically for connecting to replicas via PgBouncer or similar"
        ),
        e
      ),
      "cli:flags:add": (e) => (
        e(
          "--default-pagination-cap [int]",
          "⚡️Ensures all connections have first/last specified and are no large than this value (default: 50), set to ' -1' to disable; override via smart comment `@paginationCap 50`"
        ),
        e(
          "--graphql-depth-limit [int]",
          "⚡️Validates GraphQL queries cannot be deeper than the specified int (default: 16), set to ' -1' to disable"
        ),
        e(
          "--graphql-cost-limit [int]",
          "⚡️[experimental] Only allows queries with a computed cost below the specified int (default: 30000), set to ' -1' to disable"
        ),
        e
      ),
      "cli:library:options"(e, { config: t, cliOptions: n }) {
        const r = Object.assign({}, t.options, n),
          {
            defaultPaginationCap: i,
            graphqlDepthLimit: o,
            graphqlCostLimit: a,
            exposeGraphQLCost: s = !0,
            readOnlyConnection: l,
          } = r;
        return Object.assign({}, e, {
          defaultPaginationCap: parseInt(String(i || ""), 10) || 50,
          graphqlDepthLimit: parseInt(String(o || ""), 10) || 16,
          graphqlCostLimit: parseInt(String(a || ""), 10) || 3e4,
          exposeGraphQLCost: s,
          readOnlyConnection: l,
        });
      },
      "cli:greeting"(e, { options: t }) {
        const { license: n } = t,
          { licensedTo: r } = h(n);
        return [...e, `@graphile/pro:       v0.9.1, licensed to '${r}'`];
      },
      "postgraphile:options"(e) {
        const {
          defaultPaginationCap: t,
          graphqlDepthLimit: n,
          graphqlCostLimit: r,
          exposeGraphQLCost: i = !0,
          readOnlyConnection: o,
          license: a,
        } = e;
        let p;
        var c;
        return (
          h(a),
          o &&
            (p = y((c = o))
              ? c
              : new u.Pool(
                  "string" == typeof c ? { connectionString: c } : c || {}
                )),
          Object.assign({}, e, {
            graphqlDepthLimit: null != n ? n : 16,
            graphqlCostLimit: null != r ? r : 3e4,
            exposeGraphQLCost: i,
            readReplicaPgPool: p,
            appendPlugins: [...(e.appendPlugins || []), s.default, l.default],
            graphileBuildOptions: Object.assign({}, e.graphileBuildOptions, {
              defaultPaginationCap: null != t ? t : 50,
            }),
          })
        );
      },
      "postgraphile:httpParamsList"(e, { options: t, httpError: n }) {
        const { batchedQueryLimit: r = 10 } = t;
        if (e.length > r)
          throw n(400, `Too many batched queries, send no more than ${r}`);
        return e;
      },
      "postgraphile:validationRules"(
        e,
        { options: t, req: n, operationName: s, variables: l, meta: u }
      ) {
        const { overrideOptionsForRequest: p } = t,
          c = r.__rest(t, ["overrideOptionsForRequest"]),
          d =
            "function" == typeof p
              ? Object.assign({}, c, p(n, { options: t }))
              : c;
        return [
          ...e,
          a.default({
            operationName: s,
            variables: l,
            defaultPaginationCap: d.defaultPaginationCap,
          }),
          ...(d.graphqlDepthLimit >= 0 ? [i(d.graphqlDepthLimit)] : []),
          ...(d.graphqlCostLimit >= 0
            ? [
                o.default({
                  operationName: s,
                  variables: l,
                  costLimit: d.graphqlCostLimit,
                  onComplete(e) {
                    d.exposeGraphQLCost && u && (u.graphqlQueryCost = e);
                  },
                }),
              ]
            : []),
        ];
      },
      withPostGraphileContext(
        e,
        {
          options: {
            readReplicaPgPool: t,
            queryDocumentAst: n,
            operationName: r,
          },
        }
      ) {
        const i = n;
        if (!t) return e;
        if (i && "Document" === i.kind) {
          const n = i.definitions.find(
            (e) =>
              "OperationDefinition" === e.kind &&
              (!r || (!!e.name && e.name.value === r))
          );
          if (n) {
            const r = n.operation;
            if ("query" === r || "subscription" === r)
              return (n, r) => e(Object.assign({}, n, { pgPool: t }), r);
          }
        }
        return e;
      },
    };
    t.default = v;
  },
  function (e, t) {
    e.exports = require("tslib");
  },
  function (e, t) {
    e.exports = require("graphql-depth-limit");
  },
  function (e, t, n) {
    "use strict";
    Object.defineProperty(t, "__esModule", { value: !0 });
    const r = n(0),
      i = n(1),
      o = n(3),
      a = 0.001;
    function s(e, t, n, r = {}, i) {
      if (
        Object.keys(r).find(
          (e) =>
            ![
              "cost",
              "baseChildCost",
              "multiplier",
              "parent",
              "useParentMultiplier",
            ].includes(e)
        )
      )
        throw new Error(
          "Query cost visitor failed, invalid options passed to newNode"
        );
      const {
        cost: o = a,
        baseChildCost: s = 0,
        multiplier: l = 1,
        parent: u = null,
        useParentMultiplier: p = !0,
      } = r;
      if (!isFinite(o)) throw new Error(`Invalid cost for ${t} ${n}: '${o}'`);
      if (!isFinite(l))
        throw new Error(`Invalid multiplier for ${t} ${n}: '${l}'`);
      i &&
        i(
          `->cost=${o}, baseChildCost=${s}, multiplier=${l}, ${
            p
              ? `use parent multiplier ${u ? u.multiplier : "-"}`
              : "independent"
          }`
        );
      const c = {
        astNode: e,
        type: t,
        name: "string" == typeof n ? n : null,
        cost: o,
        baseChildCost: s,
        multiplier: l,
        parent: u,
        useParentMultiplier: p,
        children: [],
      };
      return u && u.children.push(c), c;
    }
    t.default = (e) => (t) => {
      const { coerced: n, errors: a } = i.default(e, t),
        { costLimit: l, operationName: u, onComplete: p } = e,
        c = [],
        d = {};
      function f(e) {
        if ("OperationDefinition" !== e.type)
          throw new Error(
            "Query cost visitor failed, validate called on wrong type of node"
          );
        return (function e(t, n, r = 0) {
          const {
            type: i,
            name: o,
            cost: a,
            baseChildCost: s,
            multiplier: l,
          } = t;
          if ("FragmentSpread" === i) {
            if (!o) throw new Error("Met a fragment with no name");
            const t = n[o];
            if (!t)
              throw new Error(
                `Query cost visitor failed, could not find fragment '${o}'`
              );
            return (
              a +
              t.children.reduce(
                (t, i) =>
                  t +
                  (i.useParentMultiplier ? l : 1) *
                    ((i.useParentMultiplier ? s : 0) + e(i, n, r + 1)),
                0
              )
            );
          }
          return (
            a +
            t.children.reduce(
              (t, i) =>
                t +
                (i.useParentMultiplier ? l : 1) *
                  ((i.useParentMultiplier ? s : 0) + e(i, n, r + 1)),
              0
            )
          );
        })(e, d);
      }
      let g = null,
        h = 0;
      const m = null;
      return {
        Document: {
          enter() {
            m && m("Enter document"), a && a.forEach((e) => t.reportError(e));
          },
          leave(e) {
            const n = c.length > 1 ? c.find((e) => e.name === u) : c[0];
            if (n) {
              const e = 0.2 * f(n);
              m && m(`Leave document with cost ${e}`),
                e > l &&
                  t.reportError(
                    new r.GraphQLError(
                      `This query has a computed cost of '${Math.ceil(
                        e
                      )}' which exceeds the limit of '${l}'. Try reducing the depth of the query, reducing the number of rows returned from connections or avoiding more complex fields.`,
                      n.astNode
                    )
                  ),
                "function" == typeof p && p(Math.ceil(e), e);
            } else
              t.reportError(
                new r.GraphQLError(
                  `Query cost visitor failed, could not find operation '${u}'`,
                  e
                )
              );
          },
        },
        OperationDefinition: {
          enter(e) {
            h++;
            const n = e.name ? e.name.value : "",
              i = { query: 1, mutation: 2, subscription: 10 }[e.operation];
            i
              ? ((g = s(e, "OperationDefinition", n, { multiplier: i }, m)),
                c.push(g),
                m && m(`Enter operation '${n}'`))
              : t.reportError(
                  new r.GraphQLError(
                    `Operation '${e.operation}' not understood.`,
                    e
                  )
                );
          },
          leave(e) {
            const n = e.name ? e.name.value : "";
            m && m(`Leave operation '${n}'`),
              (g = null),
              0 !== --h &&
                t.reportError(
                  new r.GraphQLError(
                    `Query cost visitor failed, '${h}' !== '0'`,
                    e
                  )
                );
          },
        },
        FragmentDefinition: {
          enter(e) {
            h++;
            const t = e.name.value;
            (g = s(e, "FragmentDefinition", t, {}, m)),
              (d[t] = g),
              m && m(`Enter fragment '${t}'`);
          },
          leave(e) {
            const n = e.name.value;
            m && m(`Leave fragment '${n}'`),
              (g = null),
              0 !== --h &&
                t.reportError(
                  new r.GraphQLError(
                    `Query cost visitor failed, '${h}' !== '0'`,
                    e
                  )
                );
          },
        },
        Field: {
          enter(e) {
            h++;
            const i = g,
              a = e.name.value,
              l = t.getParentType();
            m &&
              m(`Entering field '${a}' on type '${l ? l.name : "anonymous"}'`);
            const u = t.getFieldDef();
            if (!u) return;
            let p = {};
            try {
              p = o.getArgumentValues(u, e, n);
            } catch (e) {
              return void t.reportError(e);
            }
            const c = "function" == typeof u.getCosts ? u.getCosts(p) : {},
              d = Object.keys(c).find(
                (e) =>
                  ![
                    "cost",
                    "baseChildCost",
                    "multiplier",
                    "useParentMultiplier",
                  ].includes(e)
              );
            if (d)
              return void t.reportError(
                new r.GraphQLError(
                  `Query cost visitor failed, invalid cost key '${d}' for ${l}#${a}`,
                  e
                )
              );
            const {
              cost: f,
              baseChildCost: y,
              multiplier: v,
              useParentMultiplier: C,
            } = c;
            g = s(
              e,
              "Field",
              `${l}#${a}`,
              {
                parent: i,
                cost: f,
                baseChildCost: y,
                multiplier: v,
                useParentMultiplier: C,
              },
              m
            );
          },
          leave(e) {
            const n = e.name.value,
              r = t.getParentType();
            g && (g = g.parent),
              r && m && m(`Leaving field '${n}' on type '${r.name}'`),
              h--;
          },
        },
        FragmentSpread: {
          enter(e) {
            h++;
            const t = e.name.value;
            m && m(`Entering fragment spread '${t}'...`),
              (g = s(e, "FragmentSpread", t, { parent: g }, m));
          },
          leave() {
            (g = g ? g.parent : null),
              m && m("Leaving fragment spread..."),
              h--;
          },
        },
        InlineFragment: {
          enter(e) {
            h++,
              m && m("Entering inline fragment ..."),
              (g = s(e, "InlineFragment", "(anonymous)", { parent: g }, m));
          },
          leave() {
            (g = g ? g.parent : null),
              m && m("Leaving inline fragment..."),
              h--;
          },
        },
      };
    };
  },
  function (e, t, n) {
    "use strict";
    Object.defineProperty(t, "__esModule", { value: !0 });
    const r = n(0),
      i = n(1),
      o = n(3);
    t.default = (e) => (t) => {
      const { coerced: n, errors: a } = i.default(e, t);
      return {
        Document: {
          enter() {
            a && a.forEach((e) => t.reportError(e));
          },
        },
        Field: {
          enter(e) {
            const i = t.getFieldDef();
            if (!i) return;
            const a = i.paginationCap;
            if (null == a) return;
            if (
              !(
                !i.isPgFieldConnection ||
                (!!e.selectionSet &&
                  !!e.selectionSet.selections.find(
                    (e) => "Field" !== e.kind || "totalCount" !== e.name.value
                  ))
              )
            )
              return;
            let s;
            try {
              s = o.getArgumentValues(i, e, n) || {};
            } catch (e) {
              return void t.reportError(e);
            }
            const l = s.first,
              u = s.last;
            if (!l && !u) {
              const n = e.name.value;
              return void t.reportError(
                new r.GraphQLError(
                  `You must provide a 'first' or 'last' argument to properly paginate the '${n}' field.`,
                  e
                )
              );
            }
            const p = l || u;
            if (p < 0) {
              const n = e.name.value;
              t.reportError(
                new r.GraphQLError(
                  `Invalid pagination limit '${p}' on field '${n}', please pass an integer greater than zero.`,
                  e
                )
              );
            } else if (p > a) {
              const n = e.name.value;
              t.reportError(
                new r.GraphQLError(
                  `Invalid pagination limit '${p}' on field '${n}', please pass an integer no larger than '${a}'.`,
                  e
                )
              );
            } else;
          },
        },
      };
    };
  },
  function (e, t, n) {
    "use strict";
    Object.defineProperty(t, "__esModule", { value: !0 });
    const r = 0.95,
      i = 1,
      o = 0.12;
    t.default = (e) => {
      function t(e, t, n, a = 1e3, s = o) {
        return Object.assign({}, e, {
          getCosts(e) {
            const { first: o, last: l } = e,
              u = o || l || a,
              p = 0 === u ? 0 : Math.pow(u + 3, r) * i;
            return {
              cost: t,
              baseChildCost: s,
              multiplier: Math.max(1, n * p),
            };
          },
        });
      }
      function n(e, t) {
        const n = "number" == typeof t ? { cost: t } : t;
        return Object.assign({}, e, { getCosts: () => n });
      }
      e.hook("GraphQLObjectType:fields:field", (e, t, n) => {
        const {
          scope: { isLiveField: r, originalField: i },
        } = n;
        if (!r) return e;
        const o = e.getCosts || i.getCosts;
        return o
          ? Object.assign({}, e, {
              getCosts(...e) {
                const t = o.apply(this, e);
                return Object.assign({}, t, { cost: 10 * t.cost });
              },
            })
          : e;
      }),
        e.hook(
          "GraphQLObjectType:fields:field",
          (
            e,
            r,
            {
              scope: {
                isRootQuery: i,
                isPgFieldConnection: o,
                isPgFieldSimpleCollection: a,
                isPgConnectionTotalCountField: s,
                isPgRowConnectionType: l,
                isPageInfoHasNextPageField: u,
                isPageInfoHasPreviousPageField: p,
                isPgForwardRelationField: c,
                isPgBackwardRelationField: d,
                pgFieldIntrospection: f,
                fieldName: g,
              },
            }
          ) => {
            if (o || a)
              return f && "class" === f.kind
                ? i
                  ? t(e, 1, 1, parseInt(f.tags.unboundedAllCost, 10) || 1e5)
                  : t(
                      e,
                      0.06,
                      1.2,
                      parseInt(f.tags.unboundedRelationCost, 10) || 1e3
                    )
                : f && "procedure" === f.kind
                ? i
                  ? t(
                      e,
                      5e-4 * (f.cost || 100),
                      1,
                      parseInt(f.tags.unboundedCost, 10) || 1e3
                    )
                  : t(
                      e,
                      5e-4 * (f.cost || 100),
                      1.2,
                      parseInt(f.tags.unboundedCost, 10) || 50
                    )
                : t(e, 2, 1.2, 1e5);
            if (s)
              return f && "class" === f.kind
                ? n(e, { cost: 2, useParentMultiplier: !1 })
                : (f && f.kind, n(e, { cost: 10, useParentMultiplier: !1 }));
            if (l) {
              if ("pageInfo" === g)
                return n(e, { useParentMultiplier: !1, multiplier: 1 });
            } else {
              if (u || p) return n(e, { cost: 7 });
              if (c) return n(e, { cost: 0.06 });
              if (d) return t(e, 0.06, 1.2, 1e3);
              if (f && "procedure" === f.kind)
                return n(e, { cost: 5e-4 * (f.cost || 100) });
            }
            return e;
          }
        );
    };
  },
  function (e, t, n) {
    "use strict";
    Object.defineProperty(t, "__esModule", { value: !0 });
    t.default = (e, { defaultPaginationCap: t }) => {
      e.hook("GraphQLObjectType:fields:field", (e, t, n) => {
        const {
          scope: { isLiveField: r, originalField: i },
        } = n;
        return r
          ? Object.assign({}, e, {
              paginationCap: e.paginationCap || i.paginationCap,
            })
          : e;
      }),
        e.hook(
          "GraphQLObjectType:fields:field",
          (
            e,
            n,
            {
              scope: {
                isPgFieldConnection: r,
                isPgFieldSimpleCollection: i,
                pgFieldIntrospection: o,
                fieldName: a,
              },
            }
          ) => {
            if (
              (!r && !i) ||
              !o ||
              ("class" !== o.kind && "procedure" !== o.kind)
            )
              return e;
            const s =
              null != o.tags.paginationCap
                ? parseInt(o.tags.paginationCap, 10)
                : t;
            if (-1 === s || void 0 === s) return e;
            if ("number" != typeof s || !isFinite(s) || s < -1)
              throw new Error(
                `Invalid pagination cap for '${a}', expected a number but instead received '${s}'`
              );
            return Object.assign({}, e, {
              paginationCap: s,
              isPgFieldConnection: r,
              isPgFieldSimpleCollection: i,
            });
          }
        );
    };
  },
  function (e, t) {
    e.exports = require("pg");
  },
  function (e, t) {
    e.exports = require("crypto");
  },
]);
