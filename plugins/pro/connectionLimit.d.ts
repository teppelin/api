import { PostGraphileBaseContext, ValidationRule } from "./interfaces";
export interface PostGraphileConnectionLimitContext extends PostGraphileBaseContext {
    defaultPaginationCap?: number;
}
declare const _default: (graphileContext: PostGraphileConnectionLimitContext) => ValidationRule;
export default _default;
