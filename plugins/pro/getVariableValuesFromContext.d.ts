import { GraphQLError, ValidationContext } from "graphql";
import { PostGraphileQueryCostContext } from "./queryCost";
import { PostGraphileConnectionLimitContext } from "./connectionLimit";
export interface CalculatedVariableValues {
    errors: readonly GraphQLError[] | undefined;
    coerced: {
        [key: string]: any;
    } | undefined;
}
export default function getVariableValuesFromContext(graphileContext: PostGraphileQueryCostContext | PostGraphileConnectionLimitContext, context: ValidationContext): CalculatedVariableValues;
