import { CalculatedVariableValues } from "./getVariableValuesFromContext";
import { getArgumentValues } from "graphql/execution/values";
import { ValidationRule } from "graphql/validation/ValidationContext";
export { getArgumentValues, ValidationRule };
export interface PostGraphileBaseContext {
    operationName?: string;
    variables?: any;
    calculatedVariableValues?: CalculatedVariableValues;
}
