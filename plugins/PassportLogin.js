const { makeExtendSchemaPlugin, gql } = require("graphile-utils");
// const { sign, refresh } = require("../helpers");
const { ERROR_MESSAGE_OVERRIDES } = require("../helpers/handleErrors");

/**
 * Generate a JWT token
 *
 * @param {number} sub
 */

const PassportLoginPlugin = makeExtendSchemaPlugin((build) => ({
  typeDefs: gql`
    input RegisterInput {
      username: String!
      email: String!
      password: String!
      displayName: String
      avatarUrl: String
    }
    type RegisterPayload {
      person: Person! @pgField
    }
    input LoginInput {
      username: String!
      password: String!
    }
    type LoginPayload {
      person: Person! @pgField
    }
    type LogoutPayload {
      success: Boolean
    }
    extend type Mutation {
      """
      Use this mutation to create an account on our system. This may only be used if you are logged out.
      """
      register(input: RegisterInput!): RegisterPayload
      """
      Use this mutation to log in to your account; this login uses sessions so you do not need to take further action.
      """
      login(input: LoginInput!): LoginPayload
      """
      Use this mutation to logout from your account. Don't forget to clear the client state!
      """
      logout: LogoutPayload
    }
  `,
  resolvers: {
    Mutation: {
      async register(
        mutation,
        args,
        context,
        resolveInfo,
        { selectGraphQLResultFromTable }
      ) {
        const {
          username,
          password,
          email,
          displayName,
          avatarUrl = null,
        } = args.input;
        const { rootPgPool, login, pgClient } = context;
        try {
          // Call our login function to find out if the username/password combination exists
          const {
            rows: [details],
          } = await rootPgPool.query(
            `
          with new_person as (
            select person.* from teppelin_private.really_create_person(
              username => $1,
              email => $2,
              email_is_verified => false,
              display_name => $3,
              avatar_url => $4,
              password => $5
            ) person where not (person is null)
          ), new_session as (
            insert into teppelin_private.session (person_id)
            select person_id from new_user
            returning *
          )
          select new_person.person_id as person_id, new_session.uuid as session_id
          from new_person, new_session`,
            [username, email, displayName, avatarUrl, password]
          );

          if (!details || !details.person_id) {
            const e = new Error("Registration failed");
            e["code"] = "FFFFF";
            throw e;
          }

          if (details.session_id) {
            // Tell pg we're logged in
            // Store into transaction
            await pgClient.query(
              `select set_config('jwt.claims.session_id', $1, true)`,
              [details.session_id]
            );

            // Tell Passport.js we're logged in
            await login({ session_id: details.session_id });
          }

          // Fetch the data that was requested from GraphQL, and return it
          const sql = build.pgSql;
          const [row] = await selectGraphQLResultFromTable(
            sql.fragment`teppelin_public.person`,
            (tableAlias, sqlBuilder) => {
              sqlBuilder.where(
                sql.fragment`${tableAlias}.person_id = ${sql.value(
                  details.person_id
                )}`
              );
            }
          );

          return { data: row };
        } catch (e) {
          const { code } = e;
          const safeErrorCodes = [
            "WEAKP",
            "LOCKD",
            "EMTKN",
            ...Object.keys(ERROR_MESSAGE_OVERRIDES),
          ];
          if (safeErrorCodes.includes(code)) {
            throw e;
          } else {
            console.error(
              "Unrecognised error in PassportLoginPlugin; replacing with sanitized version"
            );
            console.error(e);
            const error = new Error("Registration failed");
            error["code"] = code;
            throw error;
          }
        }
      },
      async login(
        mutation,
        args,
        context,
        resolveInfo,
        { selectGraphQLResultFromTable }
      ) {
        const { username, password } = args.input;
        const { rootPgPool, login, pgClient } = context;
        try {
          // Call our login function to find out if the username/password combination exists
          const {
            rows: [session],
          } = await rootPgPool.query(
            "select session.* from teppelin_private.login($1, $2) session where not (session is null)",
            [username, password]
          );

          if (!session) {
            const error = new Error("Incorrect username/password");
            error["code"] = "CREDS";
            throw error;
          }

          if (session.uuid) {
            // Tell Passport.js we're logged in
            await login({ session_id: session.uuid });
          }

          // Get session_id from PG
          await pgClient.query(
            `select set_config('jwt.claims.session_id', $1, true)`,
            [session.uuid]
          );

          // Fetch the data that was requested from GraphQL, and return it
          const sql = build.pgSql;
          const [row] = await selectGraphQLResultFromTable(
            sql.fragment`teppelin_public.person`,
            (tableAlias, sqlBuilder) => {
              sqlBuilder.where(
                sql.fragment`${tableAlias}.person_id = teppelin_public.current_person_id()`
              );
            }
          );

          return { data: row };
        } catch (e) {
          const { code } = e;
          const safeErrorCodes = ["LOCKD", "CREDS"];

          if (safeErrorCodes.includes(code)) {
            throw e;
          } else {
            console.error(e);
            const error = new Error("Login failed");
            error["code"] = e.code;
            throw error;
          }
        }
      },

      async logout(_mutation, _args, context) {
        const { pgClient, logout } = context;
        await pgClient.query("select teppelin_private.logout();");
        await logout();
        return {
          success: true,
        };
      },
    },
  },
}));
module.exports = PassportLoginPlugin;
