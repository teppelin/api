const { postgraphile, makePluginHook } = require("postgraphile");
const PostGraphileConnectionFilterPlugin = require("postgraphile-plugin-connection-filter");
const PgSimplifyInflectorPlugin = require("@graphile-contrib/pg-simplify-inflector");
const PassportLogin = require("../plugins/PassportLogin");
const { handleErrors } = require("../helpers/handleErrors");

const isDev = process.env.NODE_ENV === "development";
const uuidOrNull = input => {
  if (!input) return null;
  const str = String(input);
  if (
    /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i.test(
      str
    )
  ) {
    return str;
  } else {
    return null;
  }
};

// eslint-disable-next-line
const pluginHook = makePluginHook([require("../plugins/pro").default]);

function getPostGraphileOptions(rootPgPool) {
  const options = {
    // This is for PostGraphile server plugins: https://www.graphile.org/postgraphile/plugins/
    pluginHook,

    // This is so that PostGraphile installs the watch fixtures, it's also needed to enable live queries
    ownerConnectionString: process.env.DATABASE_URL,

    // enableQueryBatching: On the client side, use something like apollo-link-batch-http to make use of this
    enableQueryBatching: true,

    // dynamicJson: instead of inputting/outputting JSON as strings, input/output raw JSON objects
    dynamicJson: true,

    // setofFunctionsContainNulls=false: reduces the number of nulls in your schema
    setofFunctionsContainNulls: false,

    // Enable GraphiQL in development
    graphiql: isDev || !!process.env.ENABLE_GRAPHIQL,
    // Use a fancier GraphiQL with `prettier` for formatting, and header editing.
    enhanceGraphiql: true,
    // Allow EXPLAIN in development (you can replace this with a callback function if you want more control)
    allowExplain: isDev,

    // Custom error handling
    handleErrors,
    watchPg: isDev,

    // include simple collections
    simpleCollections: "both",

    /*
     * Plugins to enhance the GraphQL schema, see:
     *   https://www.graphile.org/postgraphile/extending/
     */
    appendPlugins: [
      // Removes the 'ByFooIdAndBarId' from the end of relations
      PgSimplifyInflectorPlugin,

      // Adds filter argument to connection types
      PostGraphileConnectionFilterPlugin,

      // Adds the `login` mutation to enable users to log in
      PassportLogin,
    ],

    graphileBuildOptions: {
      // enable filtering on related fields
      connectionFilterRelations: true,
    },

    /*
     * Postgres transaction settings for each GraphQL query/mutation to
     * indicate to Postgres who is attempting to access the resources. These
     * will be referenced by RLS policies/triggers/etc.
     *
     * Settings set here will be set using the equivalent of `SET LOCAL`, so
     * certain things are not allowed. You can override Postgres settings such
     * as 'role' and 'search_path' here; but for settings indicating the
     * current user, session id, or other privileges to be used by RLS policies
     * the setting names must contain at least one and at most two period
     * symbols (`.`), and the first segment must not clash with any Postgres or
     * extension settings. We find `jwt.claims.*` to be a safe namespace,
     * whether or not you're using JWTs.
     */
    async pgSettings(req) {
      const sessionId = req.person && uuidOrNull(req.person.session_id);
      if (sessionId) {
        // Update the last_active timestamp (but only do it at most once every 15 seconds to avoid too much churn).
        await rootPgPool.query(
          "UPDATE teppelin_private.session SET last_active = NOW() WHERE uuid = $1 AND last_active < NOW() - INTERVAL '15 seconds'",
          [sessionId]
        );
      }
      return {
        // Everyone uses the "visitor" role currently
        role: "teppelin_visitor",

        /*
         * Note, though this says "jwt" it's not actually anything to do with
         * JWTs, we just know it's a safe namespace to use, and it means you
         * can use JWTs too, if you like, and they'll use the same settings
         * names reducing the amount of code you need to write.
         */
        "jwt.claims.session_id": sessionId,
      };
    },

    /*
     * These properties are merged into context (the third argument to GraphQL
     * resolvers). This is useful if you write your own plugins that need
     * access to, e.g., the logged in user.
     */
    async additionalGraphQLContextFromRequest(req) {
      return {
        // The current session id
        sessionId: req.person && uuidOrNull(req.person.session_id),

        // Needed so passport can write to the database
        rootPgPool,

        // Use this to tell Passport.js we're logged in
        login: person =>
          new Promise((resolve, reject) => {
            req.login(person, err => (err ? reject(err) : resolve()));
          }),

        logout: () => {
          req.logout();
          return Promise.resolve();
        },
      };
    },

    defaultPaginationCap:
      parseInt(process.env.GRAPHQL_PAGINATION_CAP || "", 10) || 50,
    graphqlDepthLimit:
      parseInt(process.env.GRAPHQL_DEPTH_LIMIT || "", 10) || 12,
    graphqlCostLimit:
      parseInt(process.env.GRAPHQL_COST_LIMIT || "", 10) || 30000,
    exposeGraphQLCost:
      (parseInt(process.env.HIDE_QUERY_COST || "", 10) || 0) < 1,
  };
  return options;
}

module.exports = function installPostGraphile(app, { rootPgPool }) {
  app.use((ctx, next) => {
    // PostGraphile deals with (req, res) but we want access to sessions from `pgSettings`,
    // so we make the ctx available on req.
    ctx.req.ctx = ctx;
    return next();
  });

  app.use(
    postgraphile(
      process.env.AUTH_DATABASE_URL,
      "teppelin_public",
      getPostGraphileOptions(rootPgPool)
    )
  );
};
