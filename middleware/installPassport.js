const passport = require("koa-passport");
const route = require("koa-route");
const { Strategy: DiscordStrategy } = require("passport-discord");
const { Strategy: GitHubStrategy } = require("passport-github");
const { Strategy: GoogleStrategy } = require("passport-google-oauth20");
const { Strategy: OAuth2Strategy } = require("passport-oauth2");

/**
 * Warn about non-set environment variables
 *
 * @param {string} app
 */
const warnEnv = (app) =>
  console.error(
    `WARNING: you've not set up the ${app} application for login; see \`README.md\` env vars table.`
  );

module.exports = function installPassport(app, { rootPgPool }) {
  passport.serializeUser((sessionObject, done) => {
    done(null, sessionObject.session_id);
  });

  passport.deserializeUser((session_id, done) => {
    done(null, { session_id });
  });

  // Add passport middleware
  app.use(passport.initialize());
  app.use(passport.session());

  app.use(
    route.get("/logout", async (ctx) => {
      ctx.logout();
      ctx.redirect("/");
    })
  );

  const strategies = {
    // GitHub strategy
    github: () =>
      new GitHubStrategy(
        {
          clientID: process.env.GITHUB_KEY,
          clientSecret: process.env.GITHUB_SECRET,
          callbackURL: `${process.env.ROOT_URL}/auth/github/callback`,
          scope: ["user", "user:email"],
          passReqToCallback: true,
        },
        async (req, accessToken, refreshToken, profile, done) => {
          try {
            if (!profile.id) {
              throw new Error(
                `OAuth strategy must return a unique id for each user`
              );
            }
            let session = null;
            if (req.person && req.person.session_id) {
              ({
                rows: [session],
              } = await rootPgPool.query(
                "select * from teppelin_private.session where uuid = $1",
                [req.person.session_id]
              ));
            }

            const {
              rows: [person],
            } = await rootPgPool.query(
              "select * from teppelin_private.link_or_register_person($1, $2, $3, $4, $5) person where not (person is null);",
              [
                session ? session.person_id : null,
                "github",
                profile.id,
                JSON.stringify({
                  username: profile.username,
                  avatar_url: profile._json.avatar_url,
                  display_name: profile.displayName || profile.username,
                  email: profile.emails.find((e) => e.primary === true).value,
                }),
                JSON.stringify({
                  accessToken,
                  refreshToken,
                }),
              ]
            );
            if (!person || !person.person_id) {
              const e = new Error("Registration failed");
              e["code"] = "FFFFF";
              throw e;
            }

            if (!session) {
              ({
                rows: [session],
              } = await rootPgPool.query(
                `insert into teppelin_private.session (person_id) values ($1) returning *`,
                [person.person_id]
              ));
            }
            if (!session) {
              const e = new Error("Failed to create session");
              e["code"] = "FFFFF";
              throw e;
            }
            done(null, { session_id: session.uuid });
          } catch (e) {
            done(e);
          }
        }
      ),
    // Discord strategy
    discord: () =>
      new DiscordStrategy(
        {
          clientID: process.env.DISCORD_KEY,
          clientSecret: process.env.DISCORD_SECRET,
          callbackURL: `${process.env.ROOT_URL}/auth/discord/callback`,
          scope: ["identify", "email"],
          passReqToCallback: true,
        },
        async (req, accessToken, refreshToken, profile, done) => {
          try {
            if (!profile.id) {
              throw new Error(
                `OAuth strategy must return a unique id for each user`
              );
            }
            let session = null;
            if (req.person && req.person.session_id) {
              ({
                rows: [session],
              } = await rootPgPool.query(
                "select * from teppelin_private.session where uuid = $1",
                [req.person.session_id]
              ));
            }

            const {
              rows: [person],
            } = await rootPgPool.query(
              "select * from teppelin_private.link_or_register_person($1, $2, $3, $4, $5) person where not (person is null);",
              [
                session ? session.person_id : null,
                "discord",
                profile.id, // unauthenticated link
                JSON.stringify({
                  username: profile.username,
                  avatar_url: `https://cdn.discordapp.com/avatars/${profile.id}/${profile.avatar}.png?size=400`,
                  display_name: profile.username,
                  email: profile.email,
                }),
                JSON.stringify({
                  accessToken,
                  refreshToken,
                }),
              ]
            );
            if (!person || !person.person_id) {
              const e = new Error("Registration failed");
              e["code"] = "FFFFF";
              throw e;
            }

            if (!session) {
              ({
                rows: [session],
              } = await rootPgPool.query(
                `insert into teppelin_private.session (person_id) values ($1) returning *`,
                [person.person_id]
              ));
            }
            if (!session) {
              const e = new Error("Failed to create session");
              e["code"] = "FFFFF";
              throw e;
            }
            done(null, { session_id: session.uuid });
          } catch (e) {
            done(e);
          }
        }
      ),
    google: () =>
      new GoogleStrategy(
        {
          clientID: process.env.GOOGLE_KEY,
          clientSecret: process.env.GOOGLE_SECRET,
          callbackURL: `${process.env.ROOT_URL}/auth/google/callback`,
          scope: ["profile", "email"],
          passReqToCallback: true,
        },
        async (req, accessToken, refreshToken, profile, done) => {
          try {
            if (!profile.id) {
              throw new Error(
                `OAuth strategy must return a unique id for each user`
              );
            }
            let session = null;
            if (req.person && req.person.session_id) {
              ({
                rows: [session],
              } = await rootPgPool.query(
                "select * from teppelin_private.session where uuid = $1",
                [req.person.session_id]
              ));
            }
            const {
              rows: [person],
            } = await rootPgPool.query(
              "select * from teppelin_private.link_or_register_person($1, $2, $3, $4, $5) person where not (person is null);",
              [
                session ? session.person_id : null,
                "google",
                profile.id, // unauthenticated link
                JSON.stringify({
                  username: profile.displayName, // this will get serialized, Google has no usernames
                  avatar_url: profile.photos.length
                    ? profile.photos[0].value
                    : null,
                  display_name: profile.displayName,
                  email: profile.emails.find((e) => e.verified === true).value,
                }),
                JSON.stringify({
                  accessToken,
                  refreshToken,
                }),
              ]
            );
            if (!person || !person.person_id) {
              const e = new Error("Registration failed");
              e["code"] = "FFFFF";
              throw e;
            }

            if (!session) {
              ({
                rows: [session],
              } = await rootPgPool.query(
                `insert into teppelin_private.session (person_id) values ($1) returning *`,
                [person.person_id]
              ));
            }
            if (!session) {
              const e = new Error("Failed to create session");
              e["code"] = "FFFFF";
              throw e;
            }
            done(null, { session_id: session.uuid });
          } catch (e) {
            done(e);
          }
        }
      ),
    anilist: () =>
      new OAuth2Strategy(
        {
          authorizationURL: "https://anilist.co/api/v2/oauth/authorize",
          tokenURL: "https://anilist.co/api/v2/oauth/token",
          clientID: process.env.ANILIST_KEY,
          clientSecret: process.env.ANILIST_SECRET,
          callbackURL: `${process.env.ROOT_URL}/auth/anilist/callback`,
          passReqToCallback: true,
        },
        async (req, accessToken, refreshToken, profile, done) => {
          try {
            if (!profile.id) {
              throw new Error(
                `OAuth strategy must return a unique id for each user`
              );
            }
            let session = null;
            if (req.person && req.person.session_id) {
              ({
                rows: [session],
              } = await rootPgPool.query(
                "select * from teppelin_private.session where uuid = $1",
                [req.person.session_id]
              ));
            } else {
              // AniList doesn't have scopes (need a profile scope)
              // so we can only link authenticated users
              throw new Error(
                "You need to authenticate in order to link an AniList account."
              );
            }

            const {
              rows: [person],
            } = await rootPgPool.query(
              "select * from teppelin_private.link_or_register_person($1, $2, $3, $4, $5) person where not (person is null);",
              [
                session ? session.person_id : null,
                "anilist",
                profile.id, // unauthenticated link
                JSON.stringify({
                  // username: profile.displayName, // Google has no usernames, dn will get serialized
                  // avatar_url: profile.photos.length ? profile.photos[0].value : null,
                  // display_name: profile.displayName,
                  // email: profile.emails.find(e => e.verified === true).value,
                }),
                JSON.stringify({
                  accessToken,
                  refreshToken,
                }),
              ]
            );
            if (!person || !person.person_id) {
              const e = new Error("Registration failed");
              e["code"] = "FFFFF";
              throw e;
            }

            if (!session) {
              ({
                rows: [session],
              } = await rootPgPool.query(
                `insert into teppelin_private.session (person_id) values ($1) returning *`,
                [person.person_id]
              ));
            }
            if (!session) {
              const e = new Error("Failed to create session");
              e["code"] = "FFFFF";
              throw e;
            }
            done(null, { session_id: session.uuid });
          } catch (e) {
            done(e);
          }
        }
      ),
  };

  // Register strategies
  if (process.env.GITHUB_KEY && process.env.GITHUB_SECRET) {
    passport.use("github", strategies.github());
  } else {
    warnEnv("GitHub");
  }
  if (process.env.DISCORD_KEY && process.env.DISCORD_SECRET) {
    passport.use("discord", strategies.discord());
  } else {
    warnEnv("Discord");
  }
  if (process.env.GOOGLE_KEY && process.env.GOOGLE_SECRET) {
    passport.use("google", strategies.google());
  } else {
    warnEnv("Google");
  }
  if (process.env.ANILIST_KEY && process.env.ANILIST_SECRET) {
    passport.use("anilist", strategies.anilist());
  } else {
    warnEnv("AniList");
  }

  // login init route
  app.use(
    route.get("/auth/:provider", (ctx, provider, next) => {
      const strategy = strategies[provider];
      // const opts = { session: false, state: ctx.query.source };

      if (!strategy) {
        return ctx.throw(400, "Unsupported authentication strategy.");
      }

      return passport.authenticate(provider)(ctx, next);
    })
  );

  // auth callback route
  app.use(
    route.get("/auth/:provider/callback", (ctx, provider, next) =>
      passport.authenticate(provider, {
        failureRedirect: "/login",
        successReturnToOrRedirect: "/",
      })(ctx, next)
    )
  );

  app.use(route.get("/teapot", (ctx) => ctx.throw(418)));
};
