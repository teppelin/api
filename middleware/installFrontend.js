const httpProxy = require("http-proxy");

// httpProxy.prototype.onError = () => {};

const { CLIENT_URL } = process.env;
if (!CLIENT_URL) {
  throw new Error("Set a 'CLIENT_URL' env variable.");
}

module.exports = function installFrontend(app, server) {
  const proxy = httpProxy.createProxyServer({
    target: CLIENT_URL,
    ws: true,
  });
  proxy.on("error", () => {});

  app.use(ctx => {
    // Bypass koa for HTTP proxying
    ctx.respond = false;

    proxy.web(ctx.req, ctx.res, {}, () => {
      ctx.res.statusCode = 503;
      ctx.res.end("Error occurred while proxying to client application.");
    });
  });
  server.on("upgrade", (req, socket, head) => {
    proxy.ws(req, socket, head);
  });
};
