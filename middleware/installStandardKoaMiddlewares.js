const cors = require("@koa/cors");
const ratelimit = require("koa-ratelimit");
const Redis = require("ioredis");

const CLOUDFLARE = Boolean(process.env.CLOUDFLARE);
const CORS_ORIGIN = process.env.CORS_ORIGIN;

module.exports = function installStandardKoaMiddlewares(app) {
  if (process.env.REDIS_URL) {
    app.use(
      ratelimit({
        db: new Redis(process.env.REDIS_URL),
        // uses CF header if we're behind them
        id: (ctx) =>
          CLOUDFLARE ? ctx.request.headers["cf-connecting-ip"] : ctx.ip
      })
    );
  } else {
    console.warn("Skipping ratelimit middleware, REDIS_URL undefined.");
  }

  // TODO: use a list of origins
  app.use(cors(CORS_ORIGIN ? { origin: CORS_ORIGIN } : {}));
};
