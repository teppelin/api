const session = require("koa-generic-session");
const redisStore = require("koa-redis");

const MILLISECOND = 1;
const SECOND = 1000 * MILLISECOND;
const MINUTE = 60 * SECOND;
const HOUR = 60 * MINUTE;
const DAY = 24 * HOUR;

const { SECRET, REDIS_URL } = process.env;
if (!SECRET) {
  throw new Error("Set a 'SECRET' env variable.");
}

const MAXIMUM_SESSION_DURATION_IN_MILLISECONDS =
  parseInt(process.env.MAXIMUM_SESSION_DURATION_IN_MILLISECONDS || "", 10) ||
  3 * DAY;

module.exports = function installSession(app) {
  app.keys = [SECRET];

  const store = REDIS_URL ? redisStore({ url: REDIS_URL }) : undefined;

  app.use(
    session({
      rolling: true,
      cookie: { maxAge: MAXIMUM_SESSION_DURATION_IN_MILLISECONDS },
      store,
    })
  );
};
