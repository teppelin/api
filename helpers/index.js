const crypto = require("crypto");
const jwt = require("jsonwebtoken");

const EXP = 3600000 * 2;

module.exports = {
  sign: (person) => ({
    accessToken: jwt.sign(
      {
        sub: person.person_id,
        username: person.username,
        displayName: person.display_name,
        avatarUrl: person.avatar_url,
        isAdmin: person.is_admin,
      },
      process.env.SECRET,
      {
        expiresIn: EXP,
        issuer: "teppelin",
      }
    ),
    expiresAt: Date.now() + EXP,
    type: "Bearer",
  }),
  refresh: () => crypto.randomBytes(20).toString("hex"),
};
